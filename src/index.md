# Ben Christel’s Homepage

<div id="tree"></div>

<div class="centered-text">

- [GitHub](https://github.com/benchristel)
- [Newsletter](https://bensguide.substack.com/)
- [Bliki](https://github.com/benchristel/benchristel.github.io/wiki)
- [Blog (defunct)](https://benchristel.github.io/)
- [email](mailto:ben.christel@gmail.com)
- [Web Portal](portal.html)

</div>

<style>

.home-link {
  display: none;
}

h1 {
  border-bottom: none;
}

div.wwwebring-widget {
  background: #eee !important;
  border: outset 2px #fff !important;
  box-shadow: 1px 1px #0005, 0 0 0 1px #0002;
}

.webring-container {
  display: table;
  margin: 2em auto;
}

#tree {
  width: 200px;
  height: 200px;
  margin: 1em auto 3em;
  background-image: url('tree-resized.jpg');
  background-position: 2px -20px;
  background-color: #fec;
  background-blend-mode: multiply;
  border: 2px solid #0004;
  border-radius: 999px;
}

</style>