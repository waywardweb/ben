# Web Portal

<input data-hypersearch type="search" placeholder="Search" style="display: block; max-width: 300px; margin-inline: auto;"/>

<div data-hypersearch-start></div>

## Projects

### Usable Software

- [The Wayward Web](https://waywardweb.org) - A webring. A collaborative effort to map the useful, human parts of the web.
- [Druthers](https://druthers.app) - Instantly grok your group's priorities with ranked-choice voting. A collaboration with [Gavin Morgan](https://gavmor.com). 
- [Culture Machine](https://benchristel.github.io/tv) - The app I wrote to break my YouTube addiction (it worked). Emotionally-stabilizing videos played continuously in an unskippable random sequence.
- [mdsite](https://benchristel.github.io/mdsite) - A modern generator for old-school static sites. It turns any tree of Markdown files into a website, complete with auto-generated navigation links. It's also the tool I used to create this webpage!
- [Taste](https://npmjs.com/package/@benchristel/taste) - Simple, speedy JavaScript test framework
- [yt](https://benchristel.github.io/yt/) - Watch YouTube without ads or tracking. Pairs great with [LeechBlock](https://www.proginosko.com/leechblock/).
- [hypersearch](https://www.npmjs.com/package/@benchristel/hypersearch) - Powers the searchbar on this page!

### Niche Software

- [Audition](https://github.com/benchristel/audition) - A command-line tool that keeps the prose description of your conlang up to date with its morphology and lexicon.
- [The One Grammar to Rule Them All](https://github.com/benchristel/OGTRTA) - The Lisp of conlangs. A syntax template for people who don't want to think about syntax anymore.
- [wwwebring](https://www.npmjs.com/package/wwwebring) - A fully clientside webring widget for static sites. Powers [the Wayward Web](https://waywardweb.org).
- [LynX](https://github.com/benchristel/LynX) - My Linux "distro." Re-skins Linux Mint to look and feel like vintage Mac OSX.
- [autokeyconf](https://github.com/benchristel/autokeyconf) - Rebinds shortcut keys system-wide on Linux.
- ["Frost" theme for Docky](https://github.com/benchristel/docky-frost) - An OSX-style dock for Linux.

### Writing

- [_Process to Processes_](https://benchristel.github.io/process-to-processes) - A draft of my upcoming book about software development
- [Ben's Guide to Software Development](https://bensguide.substack.com) - A newsletter about the book
- [Bliki](https://github.com/benchristel/benchristel.github.io/wiki) - My personal blog/wiki about software, with 250+ topic-focused pages on everything from [abstraction](https://github.com/benchristel/benchristel.github.io/wiki/Abstraction) to [wholeness](https://github.com/benchristel/benchristel.github.io/wiki/Wholeness).

## Food

- [Shopping List](shopping-list.html)

### Favorite Recipes

{{toc /recipes}}

## Music

See Channel 3 of [Culture Machine](https://benchristel.github.io/tv/)

## Software Development

- [The Art of Unix Programming](http://www.catb.org/esr/writings/taoup/html/), by Eric S. Raymond. Comprehensive, eloquent, and mind-expanding. If you only read one book on software design and development, this should be it.
- [Beautiful Software](https://www.buildingbeauty.org/beautiful-software) - a course in Christopher Alexander's methods and philosophy, for software people.
- [Refactoring.com](https://refactoring.com/) - Martin Fowler's site with descriptions of the refactorings from his book _Refactoring: Improving the Design of Existing Code_.
- [Dreamsongs.com](https://dreamsongs.com) - Richard P. Gabriel's personal site, containing his poetry and essays about Lisp, Christopher Alexander, and software.
- [MemoryManagement.org](https://www.memorymanagement.org/) - Approaches to garbage collection and more.
- [Pivotal Alumni Codex](https://alumni-codex.github.io/) - Resources compiled by former Pivots, including lists of [recommended tech videos](https://alumni-codex.github.io/engineering/tech-videos/), [Pivotal Labs-like consultancies](https://alumni-codex.github.io/consultancies/), [developer tools](https://alumni-codex.github.io/tools/) and [newsletters](https://alumni-codex.github.io/newsletters/). Maintained by [Davis W. Frank](https://dwf.bigpencil.net/).
- [The C2 Wiki](https://wiki.c2.com/) - the original Wiki. Code by Ward Cunningham, text by many contributors.
- [Eloquent Javascript](https://eloquentjavascript.net/) - web book by [Marijn Haverbeke](https://marijnhaverbeke.nl/)

### Reference

- [UnicodePlus.com](https://unicodeplus.com) - search for unicode characters.
- [man7.org](https://man7.org/) - Unix manual pages
- [A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) - on [css-tricks.com](https://css-tricks.com)

### Web Hosting

- [Neocities](https://neocities.org) - Free, indie, social web hosting. A spiritual successor to Geocities.
- [EU.org](https://nic.eu.org/) - Free subdomain names for individuals and nonprofits.
- [surge.sh](https://surge.sh) - Put any folder of HTML files on the web instantly.

### UI/UX Design

#### Theory

- [Laws of UX](https://lawsofux.com/) - Facts about humans that every designer should keep in mind!

#### Turpentine

> When art critics get together they talk about form and structure and meaning. When artists get together they talk about where you can buy cheap turpentine.
>
> — <cite>[Garson Kanin, attributed to Pablo Picasso](https://quoteinvestigator.com/2016/10/20/turpentine/)</cite>

- [GifCities](https://gifcities.org/) - Archived gifs from Geocities websites.
- [Hero Patterns](https://heropatterns.com/) - tiling SVG patterns for your website background
- [Pattern Monster](https://pattern.monster/) - even more tiling SVG patterns for your website background
- [nnnoise - SVG Noise Generator](https://www.fffuel.co/nnnoise/) - create fractal noise or turbulence dynamically with SVG. A very network-efficient replacement for PNG or JPEG textures.
- [Grainy Gradient Playground](https://grainy-gradients.vercel.app/) - dithered gradients for a retro (or is it futuristic?) look

### Tools I Use

#### Web / JavaScript Developer Tools

- [Vite](https://vitejs.dev/) - Fast, easy-to-use dev server for single-page apps. Built-in hot-reloading and TypeScript compilation, among other features.
- [Yarn](https://yarnpkg.com/) - package manager for NodeJS. Replaces `npm`.
- [Bun](https://bun.sh/) - blazing fast, but somewhat crash-prone replacement for `node` and `npm` that also implements some web APIs (but no DOM). I use Bun to run [`mdsite`](https://benchristel.github.io/mdsite) and [Taste](https://npmjs.com/package/@benchristel/taste) tests.
- [ESLint](https://eslint.org/) - I am experimenting with [eslint.style](https://eslint.style/) as a JavaScript/TypeScript formatter, replacing [Prettier](https://prettier.io/). See also [Anthony Fu's post "Why I don't use Prettier"](https://antfu.me/posts/why-not-prettier).
- [Prettier](https://prettier.io) - because it's still the easiest formatter to set up, and I agree with 95% of its choices.

#### JavaScript Libraries

- [Preact](https://preactjs.com/)
- [Marked](https://marked.js.org/) - Markdown-to-HTML converter for NodeJS

#### Editors

- [Visual Studio Code](https://code.visualstudio.com/) (vscode) - Pretty good out of the box, configurable, versatile, and free.
- [JetBrains](https://www.jetbrains.com/) - Expensive, but they come with pretty incredible code intelligence and refactoring tools. Lots of stuff you'd have to configure yourself or use plugins for in other editors "just works" in JetBrains.

### Tools I Might Use Someday

- [pnpm](https://pnpm.io/) - Replaces `npm`.
- [zed](https://zed.dev/) - An editor designed for remote pair programming. Unfortunately, there's no Linux support yet, though one can [build from source](https://zed.dev/docs/development/linux) on Linux.

## List of Lists of Links

- [Software Development](software.html)
- Life De-FAANGed
- Languages and Linguistics
- J.R.R. Tolkien
- Alexandrian Architecture and Philosophy
- [Heroes of Might and Magic IV](heroes4) - Maps and documentation for the classic computer strategy game.
- [History of Technology](techhist.html)
- [Web Archives and Archiving Tools](archiving.html)

## General Reference

- [Quote Investigator](https://quoteinvestigator.com)

## DuckDuckGo Bang Cheatsheet

Bangs are shortcuts that let you search a specific site with DuckDuckGo. They start with an exclamation point, like `!yt` for YouTube. Just include one in your search to use it.

- `!mdn` - [Mozilla Developer Network](https://developer.mozilla.org/en-US/)
- `!yt` - YouTube
- `!npm` - [NPMJS.com](https://www.npmjs.com/)
- `!g` - Google
- `!gh` - [GitHub](https://github.com/)
- `!cc` - [Creative Commons](https://creativecommons.org/) image & audio search

## Webring

<script defer src="https://cdn.jsdelivr.net/npm/wwwebring@0.1.0"></script>
<p
  class="webring-container"
  data-wwwebring="https://waywardweb.org/ring.json"
  data-wwwebring-theme="default"
  data-wwwebring-you-are-here="https://ben.waywardweb.org"
></p>

## Settings

- <label><input type="checkbox" id="autofocus-checkbox" /> Automatically focus the search field on page load.</label>

<div data-hypersearch-end></div>

<script defer type="module" src="https://cdn.jsdelivr.net/npm/@benchristel/hypersearch@0.1.0"></script>
<script>
const searchInput = document.querySelector("input[type=search]")
const autofocusCheckbox = document.getElementById("autofocus-checkbox")
if (localStorage.autofocusSearch === "true") {
  searchInput.focus()
  autofocusCheckbox.checked = true
}

autofocusCheckbox.addEventListener("change", () => {
  localStorage.autofocusSearch = autofocusCheckbox.checked
})
</script>

<style>

h1 {
  border-bottom: none;
}

div.wwwebring-widget {
  background: #eee !important;
  border: outset 2px #fff !important;
  box-shadow: 1px 1px #0005, 0 0 0 1px #0002;
}

.webring-container {
  display: table;
  margin: 2em auto;
}

</style>